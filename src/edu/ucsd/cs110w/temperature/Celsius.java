/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wce
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	} 
	
	public String toString()
	{
		// TODO: Complete this method 
		return "" + super.getValue() + " C";
	}

	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return new Fahrenheit((float)(this.getValue() * 1.8 + 32));
	}
	
	public Temperature toKelvin()
	{
		return new Kelvin((float)(this.getValue() + 273.15));
	}
	
}
