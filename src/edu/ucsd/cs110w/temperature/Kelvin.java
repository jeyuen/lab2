/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wce
 *
 */
public class Kelvin extends Temperature {

	public Kelvin(float t)
	{
		super(t);
	}
	
	public String toString()
	{
		// TODO: Complete this method
		return "" + this.getValue() + " K";
	}
	
	public Temperature toKelvin()
	{
		return this;
	}
	@Override
	public Temperature toCelsius() 
	{
		// TODO: Complete this method
		return new Celsius((float)(this.getValue() - 273.15));
	}
	
	@Override
	public Temperature toFahrenheit() 
	{
		// TODO: Complete this method
		return new Fahrenheit((float)((this.getValue() - 273.15) * 1.8 + 32));
	}

}
